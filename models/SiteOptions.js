var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * SiteOptions Model
 * ==========
 */

var SiteOptions = new keystone.List('SiteOptions', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true },
});

SiteOptions.add({
	title: { type: String, required: true },
	pageTitle: { type: String, required: false, default: '' },
	slogan: { type: String, required: false, default: 'Servicios aduanales y logísticos ACL' },
	sloganExt: { type: String, required: false, default: 'AGENCIA ADUANAL ACL - CDMX, MANZANILLO , VERACRUZ' },
	contactEmail: { type: String, required: false },
	telephone: { type: String, required: false },
	address: { type: String, required: false },
	addressCoords: { type: String, required: false },
	services: { type: Types.Relationship, ref: 'Service', many: true },
	description: {
		brief: { type: Types.Html, wysiwyg: true, height: 150 },
		extended: { type: Types.Html, wysiwyg: true, height: 400 },
	},
	twitter: { type: String, required: false},
	facebook: { type: String, required: false},
	linkedin: { type: String, required: false},
	instagram: { type: String, required: false},
	state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true },
});

SiteOptions.schema.virtual('content.full').get(function () {
	return this.content.extended || this.content.brief;
});

SiteOptions.defaultColumns = 'title, state|20%, author|20%, publishedDate|20%';
SiteOptions.register();
