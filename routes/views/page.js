var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'landing';
	locals.filters = {
		page: req.params.page,
	};

	view.on('init', function (next) {
		keystone.list('SiteOptions').model.findOne({'state': 'published'})
	  .exec(function (err, result) {
			locals.siteoptions = result
			next(err)
	  });
	});
	
	view.on('init', function (next) {
		keystone.list('Page').model.findOne({
			state: 'published',
			slug: locals.filters.page,
		}).exec(function (err, result) {
			locals.page = result
			next(err)
		});
	});

	// Load other posts
	view.on('init', function (next) {
		keystone.list('Page').model.find()
    .where('state', 'published')
    .sort('-publishedAt')
    .exec(function (err, results) {
			locals.pages = results
			next(err)
    });

	});

	// Render the view
	view.render('page', { layout: 'page' });
};
