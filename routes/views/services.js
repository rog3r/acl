var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'services';
	view.on('init', function (next) {
		keystone.list('SiteOptions').model.findOne({'state': 'published'})
	  .exec(function (err, result) {
			locals.siteoptions = result
			next(err)
	  });
	});

	view.on('init', function (next) {
		keystone.list('Service').model.find()
		.where('state', 'published')
	  .exec(function (err, result) {
			locals.services = result
			next(err)
	  });
	});

	// Load other posts
	view.on('init', function (next) {
		keystone.list('Page').model.find()
	  .where('state', 'published')
	  .sort('-publishedAt')
	  .exec(function (err, results) {
			locals.pages = results
			next(err)
  	})
	})

	// Render the view
	view.render('services', { layout: 'services' });
};
