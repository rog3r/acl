var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'service';
	view.on('init', function (next) {
		keystone.list('SiteOptions').model.findOne({'state': 'published'})
	  .exec(function (err, result) {
			locals.siteoptions = result
			next(err)
	  });
	});

	locals.filters = {
		service: req.params.service,
	};

	view.on('init', function (next) {

		keystone.list('Service').model.findOne({
			state: 'published',
			slug: locals.filters.service,
		}).exec(function (err, result) {
			locals.service = result
			next(err)
		});
	});

	// Load other posts
	view.on('init', function (next) {
		keystone.list('Page').model.find()
	  .where('state', 'published')
	  .sort('-publishedAt')
	  .exec(function (err, results) {
			locals.pages = results
			next(err)
  	})
	})

	// Render the view
	view.render('service', { layout: 'service' });
};
